'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">lasersoft-ui documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-7bcbb8f5f8d0473376d6d525b6de8b6692281cab85b1320086b3b667a2e4e1788edb8c698847822281ae44c99089e62af8453ffa5b5d32a7297d1abc35938147"' : 'data-target="#xs-components-links-module-AppModule-7bcbb8f5f8d0473376d6d525b6de8b6692281cab85b1320086b3b667a2e4e1788edb8c698847822281ae44c99089e62af8453ffa5b5d32a7297d1abc35938147"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-7bcbb8f5f8d0473376d6d525b6de8b6692281cab85b1320086b3b667a2e4e1788edb8c698847822281ae44c99089e62af8453ffa5b5d32a7297d1abc35938147"' :
                                            'id="xs-components-links-module-AppModule-7bcbb8f5f8d0473376d6d525b6de8b6692281cab85b1320086b3b667a2e4e1788edb8c698847822281ae44c99089e62af8453ffa5b5d32a7297d1abc35938147"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Directive2Module.html" data-type="entity-link" >Directive2Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Directive2Module-bff258783863a38b311dbc7def7f4518df1a2bb94af8363a72bbb2d4fb4b61f719d28540fe6fb445c03f297370ff4ac83eb6cb9e583b9a36770c996318d63761"' : 'data-target="#xs-components-links-module-Directive2Module-bff258783863a38b311dbc7def7f4518df1a2bb94af8363a72bbb2d4fb4b61f719d28540fe6fb445c03f297370ff4ac83eb6cb9e583b9a36770c996318d63761"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Directive2Module-bff258783863a38b311dbc7def7f4518df1a2bb94af8363a72bbb2d4fb4b61f719d28540fe6fb445c03f297370ff4ac83eb6cb9e583b9a36770c996318d63761"' :
                                            'id="xs-components-links-module-Directive2Module-bff258783863a38b311dbc7def7f4518df1a2bb94af8363a72bbb2d4fb4b61f719d28540fe6fb445c03f297370ff4ac83eb6cb9e583b9a36770c996318d63761"' }>
                                            <li class="link">
                                                <a href="components/Directive2Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Directive2Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Directive2RoutingModule.html" data-type="entity-link" >Directive2RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Directive3Module.html" data-type="entity-link" >Directive3Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Directive3Module-c0a99326fb55f4f1a1f390df748fe721246bb6886790945a2e7ce0606996d52dbeb91631f450f3ac15146062cc0879206de18f4267494c92698e6e5a1a571b91"' : 'data-target="#xs-components-links-module-Directive3Module-c0a99326fb55f4f1a1f390df748fe721246bb6886790945a2e7ce0606996d52dbeb91631f450f3ac15146062cc0879206de18f4267494c92698e6e5a1a571b91"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Directive3Module-c0a99326fb55f4f1a1f390df748fe721246bb6886790945a2e7ce0606996d52dbeb91631f450f3ac15146062cc0879206de18f4267494c92698e6e5a1a571b91"' :
                                            'id="xs-components-links-module-Directive3Module-c0a99326fb55f4f1a1f390df748fe721246bb6886790945a2e7ce0606996d52dbeb91631f450f3ac15146062cc0879206de18f4267494c92698e6e5a1a571b91"' }>
                                            <li class="link">
                                                <a href="components/Directive3Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Directive3Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Directive3RoutingModule.html" data-type="entity-link" >Directive3RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DirectivesModule.html" data-type="entity-link" >DirectivesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DirectivesModule-802055d4efb02bd1ced5edf439e5ff903a2c9db103177bbc7e253d573aa019725eaa2be8780ecdf0589374288fc4ef59ad31e052bd531541e19d41eef6d08ba1"' : 'data-target="#xs-components-links-module-DirectivesModule-802055d4efb02bd1ced5edf439e5ff903a2c9db103177bbc7e253d573aa019725eaa2be8780ecdf0589374288fc4ef59ad31e052bd531541e19d41eef6d08ba1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DirectivesModule-802055d4efb02bd1ced5edf439e5ff903a2c9db103177bbc7e253d573aa019725eaa2be8780ecdf0589374288fc4ef59ad31e052bd531541e19d41eef6d08ba1"' :
                                            'id="xs-components-links-module-DirectivesModule-802055d4efb02bd1ced5edf439e5ff903a2c9db103177bbc7e253d573aa019725eaa2be8780ecdf0589374288fc4ef59ad31e052bd531541e19d41eef6d08ba1"' }>
                                            <li class="link">
                                                <a href="components/DirectivesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DirectivesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DirectivesRoutingModule.html" data-type="entity-link" >DirectivesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Pipes2Module.html" data-type="entity-link" >Pipes2Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Pipes2Module-b4ac9e6f178964fce3eafc3647fca4d7cc299ddacb22b61ce4c8b2f321c431491c01bb027ade13157096674bdecc81d79297d40536a071f683435d1a67fce9df"' : 'data-target="#xs-components-links-module-Pipes2Module-b4ac9e6f178964fce3eafc3647fca4d7cc299ddacb22b61ce4c8b2f321c431491c01bb027ade13157096674bdecc81d79297d40536a071f683435d1a67fce9df"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Pipes2Module-b4ac9e6f178964fce3eafc3647fca4d7cc299ddacb22b61ce4c8b2f321c431491c01bb027ade13157096674bdecc81d79297d40536a071f683435d1a67fce9df"' :
                                            'id="xs-components-links-module-Pipes2Module-b4ac9e6f178964fce3eafc3647fca4d7cc299ddacb22b61ce4c8b2f321c431491c01bb027ade13157096674bdecc81d79297d40536a071f683435d1a67fce9df"' }>
                                            <li class="link">
                                                <a href="components/Pipes2Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Pipes2Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Pipes2RoutingModule.html" data-type="entity-link" >Pipes2RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Pipes3Module.html" data-type="entity-link" >Pipes3Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Pipes3Module-58a6f682cd90c28b11aa4f38c4fc2d2d90277848399f7b30d23fd089f67e08a7e091e14f0de0538338c762456c5ddcc7116997648c4938a15ce026fda10b8586"' : 'data-target="#xs-components-links-module-Pipes3Module-58a6f682cd90c28b11aa4f38c4fc2d2d90277848399f7b30d23fd089f67e08a7e091e14f0de0538338c762456c5ddcc7116997648c4938a15ce026fda10b8586"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Pipes3Module-58a6f682cd90c28b11aa4f38c4fc2d2d90277848399f7b30d23fd089f67e08a7e091e14f0de0538338c762456c5ddcc7116997648c4938a15ce026fda10b8586"' :
                                            'id="xs-components-links-module-Pipes3Module-58a6f682cd90c28b11aa4f38c4fc2d2d90277848399f7b30d23fd089f67e08a7e091e14f0de0538338c762456c5ddcc7116997648c4938a15ce026fda10b8586"' }>
                                            <li class="link">
                                                <a href="components/Pipes3Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Pipes3Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Pipes3RoutingModule.html" data-type="entity-link" >Pipes3RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PipesModule.html" data-type="entity-link" >PipesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PipesModule-638737c056f865595afdd18cabc71d4fad326da22a6bd14ea38485ceba48582c901e01c879eebf6390efbc800625000a23eefe1680e84469b145499a587485e6"' : 'data-target="#xs-components-links-module-PipesModule-638737c056f865595afdd18cabc71d4fad326da22a6bd14ea38485ceba48582c901e01c879eebf6390efbc800625000a23eefe1680e84469b145499a587485e6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PipesModule-638737c056f865595afdd18cabc71d4fad326da22a6bd14ea38485ceba48582c901e01c879eebf6390efbc800625000a23eefe1680e84469b145499a587485e6"' :
                                            'id="xs-components-links-module-PipesModule-638737c056f865595afdd18cabc71d4fad326da22a6bd14ea38485ceba48582c901e01c879eebf6390efbc800625000a23eefe1680e84469b145499a587485e6"' }>
                                            <li class="link">
                                                <a href="components/PipesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PipesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PipesRoutingModule.html" data-type="entity-link" >PipesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' : 'data-target="#xs-components-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' :
                                            'id="xs-components-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AccordionGroupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BodyComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BodyComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ColComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ColComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GmapComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GmapComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MapquestStaticComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapquestStaticComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RowComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RowComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SeparatorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SeparatorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TextComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TextComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/Weather2Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Weather2Component</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WeatherComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WeatherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' : 'data-target="#xs-directives-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' :
                                        'id="xs-directives-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' }>
                                        <li class="link">
                                            <a href="directives/BorderDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BorderDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/FlexColDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FlexColDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/FlexDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FlexDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/FocusDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FocusDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/HelloDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HelloDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfLoggedDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfLoggedDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfRoleIsDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfRoleIsDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfSigninDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfSigninDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/LinkDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LinkDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/LoaderDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoaderDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MarginDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MarginDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/PadDirectives.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PadDirectives</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/StopPropagationDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StopPropagationDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/UrlDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UrlDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' : 'data-target="#xs-pipes-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' :
                                            'id="xs-pipes-links-module-SharedModule-015b3a72904dc5f57b41a64224e7af3bbda80da0d77631ca7e61444b5247cbbaeed50dfe0ad4136ceb16f828b76c1206a2986a31173ff79f73dae0882ae18940"' }>
                                            <li class="link">
                                                <a href="pipes/FilterByGenderPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FilterByGenderPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/FilterByTextPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FilterByTextPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/FormatGbPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormatGbPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/GetUserHumanPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GetUserHumanPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/GetUserPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GetUserPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/MapquestPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapquestPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/SortPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SortPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/TimeAgoPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TimeAgoPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitDemo1Module.html" data-type="entity-link" >UikitDemo1Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitDemo1Module-bf07370107a40911a2a9784f37d71274687fba175afdc7f27b1c5a96064dfa4979f333d5560acf89acbd805ec40e5df6f19ba61fda84280bf5f506da49482354"' : 'data-target="#xs-components-links-module-UikitDemo1Module-bf07370107a40911a2a9784f37d71274687fba175afdc7f27b1c5a96064dfa4979f333d5560acf89acbd805ec40e5df6f19ba61fda84280bf5f506da49482354"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitDemo1Module-bf07370107a40911a2a9784f37d71274687fba175afdc7f27b1c5a96064dfa4979f333d5560acf89acbd805ec40e5df6f19ba61fda84280bf5f506da49482354"' :
                                            'id="xs-components-links-module-UikitDemo1Module-bf07370107a40911a2a9784f37d71274687fba175afdc7f27b1c5a96064dfa4979f333d5560acf89acbd805ec40e5df6f19ba61fda84280bf5f506da49482354"' }>
                                            <li class="link">
                                                <a href="components/UikitDemo1Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UikitDemo1Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitDemo1RoutingModule.html" data-type="entity-link" >UikitDemo1RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UikitDemo2Module.html" data-type="entity-link" >UikitDemo2Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitDemo2Module-72618518f6217007c5611d4432c072069f51e7708323662ed6d6ee9e9b9be4806df457083802f2d5b378455f499be3e9337f4cf8839269f874eda7512a334655"' : 'data-target="#xs-components-links-module-UikitDemo2Module-72618518f6217007c5611d4432c072069f51e7708323662ed6d6ee9e9b9be4806df457083802f2d5b378455f499be3e9337f4cf8839269f874eda7512a334655"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitDemo2Module-72618518f6217007c5611d4432c072069f51e7708323662ed6d6ee9e9b9be4806df457083802f2d5b378455f499be3e9337f4cf8839269f874eda7512a334655"' :
                                            'id="xs-components-links-module-UikitDemo2Module-72618518f6217007c5611d4432c072069f51e7708323662ed6d6ee9e9b9be4806df457083802f2d5b378455f499be3e9337f4cf8839269f874eda7512a334655"' }>
                                            <li class="link">
                                                <a href="components/UikitDemo2Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UikitDemo2Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitDemo2RoutingModule.html" data-type="entity-link" >UikitDemo2RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UikitDemo3Module.html" data-type="entity-link" >UikitDemo3Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitDemo3Module-4c0f5e72749c3f4d74a2175b0ca7ff09e2371274386daa2ae9ed8d80b7e26684aabe008f18f41310825a9cb6ae59d22fd16c64bb73da5aed24b7e36dc4023679"' : 'data-target="#xs-components-links-module-UikitDemo3Module-4c0f5e72749c3f4d74a2175b0ca7ff09e2371274386daa2ae9ed8d80b7e26684aabe008f18f41310825a9cb6ae59d22fd16c64bb73da5aed24b7e36dc4023679"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitDemo3Module-4c0f5e72749c3f4d74a2175b0ca7ff09e2371274386daa2ae9ed8d80b7e26684aabe008f18f41310825a9cb6ae59d22fd16c64bb73da5aed24b7e36dc4023679"' :
                                            'id="xs-components-links-module-UikitDemo3Module-4c0f5e72749c3f4d74a2175b0ca7ff09e2371274386daa2ae9ed8d80b7e26684aabe008f18f41310825a9cb6ae59d22fd16c64bb73da5aed24b7e36dc4023679"' }>
                                            <li class="link">
                                                <a href="components/UikitDemo3Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UikitDemo3Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitDemo3RoutingModule.html" data-type="entity-link" >UikitDemo3RoutingModule</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Auth.html" data-type="entity-link" >Auth</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Clouds.html" data-type="entity-link" >Clouds</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Coord.html" data-type="entity-link" >Coord</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Main.html" data-type="entity-link" >Main</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Meteo.html" data-type="entity-link" >Meteo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Sys.html" data-type="entity-link" >Sys</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Weather.html" data-type="entity-link" >Weather</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Widget.html" data-type="entity-link" >Widget</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Wind.html" data-type="entity-link" >Wind</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});