import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';

export interface Auth {
  token: string;
  role: 'admin' | 'moderator'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data$ = new BehaviorSubject<Auth | null>(null);

  login() {
    // http
    this.data$.next({ token: 'abc123', role: 'admin'})
  }

  logout() {
    this.data$.next(null)
  }

  get ifLogged$() {
    return this.data$
      .pipe(
        map(d => !!d)
      )
  }
  get role() {
    return this.data$
      .pipe(
        map(d => d?.role)
      )
  }
}
