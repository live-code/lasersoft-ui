import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo2RoutingModule } from './uikit-demo2-routing.module';
import { UikitDemo2Component } from './uikit-demo2.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    UikitDemo2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    UikitDemo2RoutingModule
  ]
})
export class UikitDemo2Module { }
