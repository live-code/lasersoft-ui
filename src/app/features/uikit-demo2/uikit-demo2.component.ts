import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit-demo2',
  template: `
    <app-gmap [coords]="coords" [clustering]="true" [zoom]="zoom"></app-gmap>
    <button (click)="coords = { lat: 43, lng: 13 }">Italy</button>
    <button (click)="coords = { lat: 55, lng: 11 }">ABC</button>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    
    <hr>
    <app-mapquest-static [city]="city" [zoom]="zoom"></app-mapquest-static>

    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="city = 'roma'">Roma</button>
    <button (click)="city = 'palermo'">Palermo</button>

  `,
  styles: [
  ]
})
export class UikitDemo2Component {
  city = 'Milan';
  coords: { lat: number, lng: number} = { lat: -34.397, lng: 150.644 }
  zoom = 7;
}
