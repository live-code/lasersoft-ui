import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo2Component } from './uikit-demo2.component';

const routes: Routes = [{ path: '', component: UikitDemo2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo2RoutingModule { }
