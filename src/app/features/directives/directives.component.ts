import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-directives',
  template: `
    
    
    Visita il sito <span appUrl="http://www.google.com">Google</span> oppure
    <span appLink="http://www.facebook.com">facebook</span>

   
    <h1>Auth Example</h1>
    <pre>{{(authService.data$ | async)?.token}}</pre>
    <button (click)="authService.login()">Login</button>
    <button
      (click)="authService.logout()"
      appIfLogged
    >Logout</button>
    
    <button
        (click)="authService.logout()"
        *appIfSignin
      >Quit</button>
     
    <button
        (click)="authService.logout()"
        *appIfRoleIs="'admin'"
      >Quit for admins</button>
    <hr>
    
    
    <h1>Attribute Directives</h1>
    <div [appPad]="2" color="red">Padding Example</div>
    <button (click)="value = value + 1">+</button>
    <button (click)="borderValue = 'dotted'">dotted</button>
    <button (click)="borderValue = 'solid'">solid</button>
    <div [appMargin]="value" 
         [appBorder]="borderValue">Margin + border Example</div>
    
    <div 
      class="alert alert-danger" 
      [style.fontSize.px]="10" 
      [innerHTML]="label">....</div>
    
    <div appHello #h="appHello">
      xyz bla bla
      <div>{{h.class}}</div>
    </div>
    
    
    <form #f="ngForm" (submit)="save(f.value)">
      <input type="text" ngModel name="username" required #userRef="ngModel">
      {{userRef.valid}} - {{userRef.errors | json}}
      <input type="text" ngModel name="password">
      <button type="submit" [disabled]="f.invalid">SAVE</button>
    </form>
   
  `,
})
export class DirectivesComponent implements OnInit {
  label = 'pluto';
  value = 1;
  borderValue = 'dashed'

  constructor(public authService: AuthService) {
  }

  ngOnInit(): void {
  }

  save(formData: any) {
    console.log(formData)
  }
}
