import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';
import { MapquestStaticComponent } from '../../shared/components/mapquest-static.component';
import { CardComponent } from '../../shared/components/card/card.component';
export interface Widget {
  id: number,
  type: string;
  data: any;
}
@Component({
  selector: 'app-directive3',
  template: `
    <p>directive3 works!</p>
    
    <div appFlex="sm">
      <div appFlexCol="6">left</div>
      <div [appFlexCol]="6">right</div>
    </div>
    
    <app-row mq="sm">
      <app-col [cols]="8">
        <div *ngFor="let data of config">
          <div [appLoader]="data"></div>
        </div>
      </app-col>
      <app-col [cols]="4">
        <app-mapquest-static city="Palermo"></app-mapquest-static>
        <app-gmap [coords]="{ lat: 43, lng: 13}"></app-gmap>
      </app-col>
    </app-row>
    <h1>Hello</h1>
      
  `,
})
export class Directive3Component implements OnInit {
  params = 'gmap';
  config: Widget[] = [
    { id: 1, type: 'map', data: { city: 'palermo'}},
    { id: 2, type: 'card', data: { title: 'ciao', icon: 'fa fa-facebook'}},
    { id: 2, type: 'weather2', data: { city: 'Milano'}},
  ]


  constructor(

  ) {


  }

  ngOnInit(): void {
  }

}
