import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directive3Component } from './directive3.component';

const routes: Routes = [{ path: '', component: Directive3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directive3RoutingModule { }
