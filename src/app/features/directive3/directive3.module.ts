import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Directive3RoutingModule } from './directive3-routing.module';
import { Directive3Component } from './directive3.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    Directive3Component
  ],
  imports: [
    CommonModule,
    Directive3RoutingModule,
    SharedModule
  ]
})
export class Directive3Module { }
