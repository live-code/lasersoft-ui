import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CardComponent } from '../../shared/components/card/card.component';

@Component({
  selector: 'app-uikit-demo3',
  template: `
    
    <app-accordion>
      <app-accordion-group title="one">
        <app-body>...</app-body>
      </app-accordion-group>
      <app-accordion-group title="two">
        <app-body>...</app-body>
      </app-accordion-group>
      <app-accordion-group title="three">
        <app-body>...</app-body>
      </app-accordion-group>
      
      <app-accordion-group 
        *ngFor="let u of users"
        [title]="'user' + u"
      >
        content {{u}}
      </app-accordion-group>
    </app-accordion>
    
    <!--
    <app-card></app-card>
    <app-card></app-card>
    <app-card></app-card>-->
  `,
  styles: [
  ]
})
export class UikitDemo3Component {
  // @ViewChild('host') host!: ElementRef<HTMLDivElement>
  // @ViewChildren('host') hosts!: QueryList<ElementRef<HTMLDivElement>>;
  @ViewChildren(CardComponent) cards!: QueryList<CardComponent>;
  users = [1, 2, 3, 4]
}
