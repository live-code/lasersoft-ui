import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo3RoutingModule } from './uikit-demo3-routing.module';
import { UikitDemo3Component } from './uikit-demo3.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    UikitDemo3Component
  ],
  imports: [
    CommonModule,
    UikitDemo3RoutingModule,
    SharedModule
  ]
})
export class UikitDemo3Module { }
