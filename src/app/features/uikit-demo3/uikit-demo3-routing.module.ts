import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo3Component } from './uikit-demo3.component';
import { AccordionGroupComponent } from '../../shared/components/accordion/accordion-group.component';

const routes: Routes = [{ path: '', component: UikitDemo3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo3RoutingModule { }
