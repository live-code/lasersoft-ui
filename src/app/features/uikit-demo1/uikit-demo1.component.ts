import { AfterViewInit, Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { debounceTime, distinctUntilChanged, fromEvent } from 'rxjs';

@Component({
  selector: 'app-uikit-demo1',
  // encapsulation: ViewEncapsulation.None,
  template: `
   <app-text [val]="1">text example</app-text>
   <app-text [val]="3" color="danger">text example</app-text>
   
   
   <app-separator></app-separator>
   <app-separator color="red" type="dashed"></app-separator>
   
    <input #inputRef type="text" placeholder="search your city">
    <app-weather2 [city]="city"></app-weather2>
    <app-weather [city]="city" [titleColor]="color" [config]="cfg"></app-weather>
    <button (click)="cfg.unit = 'imperial'">imperial</button>
    <button (click)="cfg = { unit: 'imperial'}">imperial</button>
    <button (click)="color = 'cyan'">cyan</button>
    <button (click)="color = 'red'">red</button>
    <button (click)="city = 'milano'">milano</button>
    <button (click)="city = 'rome'">rome</button>
    
    <hr>
    
    <app-card title="pippo" *ngIf="show">
      <app-body>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque culpa dicta ipsam itaque modi quaerat quo saepe sit suscipit veritatis. Animi atque possimus vero voluptas? Alias, dicta, minima? Repudiandae, sapiente.
      </app-body>
      <app-footer>
        <button>one</button>
        <button>two</button>
      </app-footer>
    </app-card>
    
    <button (click)="show = true">show modal</button>
    <button (click)="show = false">hide modal</button>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolorum ducimus ea excepturi facilis illum impedit iste itaque laborum minima nobis praesentium quas quia recusandae repellendus, voluptas voluptatem voluptates voluptatum!
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolorum ducimus ea excepturi facilis illum impedit iste itaque laborum minima nobis praesentium quas quia recusandae repellendus, voluptas voluptatem voluptates voluptatum!
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolorum ducimus ea excepturi facilis illum impedit iste itaque laborum minima nobis praesentium quas quia recusandae repellendus, voluptas voluptatem voluptates voluptatum!
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolorum ducimus ea excepturi facilis illum impedit iste itaque laborum minima nobis praesentium quas quia recusandae repellendus, voluptas voluptatem voluptates voluptatum!
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolorum ducimus ea excepturi facilis illum impedit iste itaque laborum minima nobis praesentium quas quia recusandae repellendus, voluptas voluptatem voluptates voluptatum!
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolorum ducimus ea excepturi facilis illum impedit iste itaque laborum minima nobis praesentium quas quia recusandae repellendus, voluptas voluptatem voluptates voluptatum!
  `,
  styles: [`
    .mymodal {
      background-color: cyan;
      position: fixed;
      width: 300px;
      top: 100px;
      right: 0;
      left: 0;
    }
  `]
})
export class UikitDemo1Component implements AfterViewInit {
  @ViewChild('inputRef') input!: ElementRef<HTMLInputElement>;
  show = false;
  city: string | null = 'palermo'
  color = 'purple'
  cfg = { unit: 'metric'}

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(
        (e: Event) => this.city = (e.target as HTMLInputElement).value
      )
    this.input.nativeElement.focus()
  }

  openUrl(): void {
    alert('open url')
  }

  render() {
    console.log('RENDER: uikit-demo1')
  }
}
