import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo1RoutingModule } from './uikit-demo1-routing.module';
import { UikitDemo1Component } from './uikit-demo1.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UikitDemo1Component
  ],
  imports: [
    CommonModule,
    UikitDemo1RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class UikitDemo1Module { }
