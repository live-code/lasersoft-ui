import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Directive2Component } from './directive2.component';

const routes: Routes = [{ path: '', component: Directive2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Directive2RoutingModule { }
