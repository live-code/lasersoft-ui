import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directive2',
  template: `
    
    <h1>focus</h1>
    <input type="text" appFocus>
    <h1>stopPropagation</h1>
    <div (click)="parent()">
      <pre>Introduzione</pre>
      <li appStopPropagation (click)="child()">1</li>
      <li appStopPropagation (click)="child()">1</li>
      <li appStopPropagation (click)="child()">1</li>
    </div>
  `,
  styles: [
  ]
})
export class Directive2Component {

  parent() {
    console.log('parent')
  }

  child() {
    console.log('child')
  }
}
