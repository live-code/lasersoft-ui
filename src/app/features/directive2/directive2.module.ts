import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Directive2RoutingModule } from './directive2-routing.module';
import { Directive2Component } from './directive2.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    Directive2Component
  ],
  imports: [
    CommonModule,
    Directive2RoutingModule,
    SharedModule
  ]
})
export class Directive2Module { }
