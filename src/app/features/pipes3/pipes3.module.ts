import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Pipes3RoutingModule } from './pipes3-routing.module';
import { Pipes3Component } from './pipes3.component';


@NgModule({
  declarations: [
    Pipes3Component
  ],
  imports: [
    CommonModule,
    Pipes3RoutingModule
  ]
})
export class Pipes3Module { }
