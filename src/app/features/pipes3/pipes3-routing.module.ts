import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Pipes3Component } from './pipes3.component';

const routes: Routes = [{ path: '', component: Pipes3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pipes3RoutingModule { }
