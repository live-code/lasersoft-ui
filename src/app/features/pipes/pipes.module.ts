import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PipesRoutingModule } from './pipes-routing.module';
import { PipesComponent } from './pipes.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    PipesComponent
  ],
  imports: [
    CommonModule,
    PipesRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class PipesModule { }
