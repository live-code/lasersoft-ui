import { Component } from '@angular/core';

@Component({
  selector: 'app-pipes',
  template: `
    <input type="text" ngModel>
    <button (click)="gb = 10">CLICKME</button>
    <hr>
    {{gb | formatGb: 'mb'}}
    {{gb | formatGb: 'kb'}}
    {{gb | formatGb: 'byte'}}
    
    <img [src]="city | mapquest">
    <img [src]="'milano' | mapquest: 10 : '200,200'">
    <img [src]="'milano' | mapquest: zoom : '400,300'">
    <button (click)="zoom = zoom + 1">+</button>
   
  `,
})
export class PipesComponent  {
  gb = 8
  city = 'Rome'
  zoom = 10;

  formatGbToMb(value: number) {
    console.log('render')
    return value * 1000;
  }
}
