import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Pipes2RoutingModule } from './pipes2-routing.module';
import { Pipes2Component } from './pipes2.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    Pipes2Component
  ],
  imports: [
    CommonModule,
    Pipes2RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class Pipes2Module { }
