import { Component, OnInit } from '@angular/core';

export interface User {
  id: number;
  name: string;
  age: number;
  gender: 'M' | 'F';
}

@Component({
  selector: 'app-pipes2',
  template: `
    
    <input type="text" [(ngModel)]="currentFilterName">
    
    <select [(ngModel)]="currentFilterGender">
      <option value="">All</option>
      <option value="M">Male</option>
      <option value="F">Female</option>    
    </select>
    
    <select [(ngModel)]="currentSort">
      <option value="ASC">ASC</option>
      <option value="DESC">DESC</option>    
    </select>
    
    <h1>Filters: {{currentFilterName}} - {{currentFilterGender}} - {{currentSort}}</h1>
    <hr>
    
    
    <li 
      *ngFor="let user of users 
      | filterByGender: currentFilterGender 
      | filterByText: currentFilterName
      | sort: currentSort
    ">
     {{user.name}} - {{user.age}} - {{user.gender}}
    </li>
    
    <hr>
    {{1530442212000 | timeAgo}} <br>
    {{1600442212000 | timeAgo}} <br>
    {{today | timeAgo}}
    
    <hr>
    {{(userId | getUser | async)?.name}}
    {{userId | getUserHuman | async}}
  `,
})
export class Pipes2Component {
  currentFilterName = ''
  currentFilterGender: 'M' | 'F' | '' = ''
  currentSort: 'ASC' | 'DESC' = 'ASC'
  today = new Date()
  userId = 5;

  users: User[] = [
    { id: 1, name: 'Silvio', age: 20, gender: 'M' },
    { id: 2, name: 'Ciccio', age: 30, gender: 'M' },
    { id: 3, name: 'Dilvia', age: 25, gender: 'F' },
  ]

}
