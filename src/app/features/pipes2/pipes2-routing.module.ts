import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Pipes2Component } from './pipes2.component';

const routes: Routes = [{ path: '', component: Pipes2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pipes2RoutingModule { }
