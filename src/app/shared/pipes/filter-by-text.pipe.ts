import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../features/pipes2/pipes2.component';

@Pipe({
  name: 'filterByText'
})
export class FilterByTextPipe implements PipeTransform {

  transform(items: User[], textToFind: string): User[] {
    return items.filter(item => {
      return item.name.toLowerCase().includes(textToFind.toLowerCase())
    });
  }

}
