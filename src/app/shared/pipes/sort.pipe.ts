import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../features/pipes2/pipes2.component';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(items: User[], orderBy: 'ASC' | 'DESC'): User[] {
    const sortResult = items.sort((a, b) => a.name.localeCompare(b.name));
    return orderBy === 'ASC' ? sortResult : sortResult.reverse();
  }

}
