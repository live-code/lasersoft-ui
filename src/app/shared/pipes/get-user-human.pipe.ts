import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { User } from '../../features/pipes2/pipes2.component';

@Pipe({
  name: 'getUserHuman'
})
export class GetUserHumanPipe implements PipeTransform {

  constructor(private http: HttpClient) {
  }
  transform(userId: number): Observable<string> {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/5')
      .pipe(
        map(user => `${user.name} (id: ${user.id}) `)
      )
  }
}
