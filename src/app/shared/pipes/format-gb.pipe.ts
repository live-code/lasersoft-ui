import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatGb'
})
export class FormatGbPipe implements PipeTransform {
  transform(value: number, format: 'mb' | 'kb' | 'byte' = 'mb'): number {
    switch (format) {
      case 'mb': return value * 1000;
      case 'kb': return value * 1000000;
      case 'byte': return value * 1000000000;
    }
  }
}
