import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../features/pipes2/pipes2.component';

@Pipe({
  name: 'filterByGender'
})
export class FilterByGenderPipe implements PipeTransform {

  transform(
    items: User[],
    gender: 'M' | 'F' | '' = '',
  ): User[] {
    if (gender === '') {
      return items;
    }
    return items.filter(item => item.gender === gender);
  }

}
