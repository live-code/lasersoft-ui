import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../features/pipes2/pipes2.component';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Pipe({
  name: 'getUser'
})
export class GetUserPipe implements PipeTransform {

  constructor(private http: HttpClient) {
  }
  transform(userId: number): Observable<User> {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/5')
  }

}
