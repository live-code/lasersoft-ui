import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapquest'
})
export class MapquestPipe implements PipeTransform {
  transform(
    city: string,
    zoom: number = 7,
    size: string = '300,200'
  ): string {
    return `https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${city}&size=${size}&zoom=${zoom}`;
  }
}
