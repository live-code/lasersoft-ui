import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-separator',
  template: `
    <div 
      class="separator"
      [style.border-color]="color"
      [style.border-style]="type"
    ></div>
  `,
  styles: [`
    .separator {
      margin: 20px 0;
      border-top: 1px dashed black;
    }
  `]
})
export class SeparatorComponent implements OnInit {
  @Input() color: string = 'black';
  @Input() type: 'solid' | 'dashed' = 'solid';
  constructor() { }

  ngOnInit(): void {
  }

}
