import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { google } from 'google-maps';

@Component({
  selector: 'app-gmap',
  template: `
    <pre>{{coords | json}}</pre>
    <div #host style="height: 300px"></div>
  `,
  styles: [
  ]
})
export class GmapComponent implements OnChanges {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>;
  @Input() coords: { lat: number, lng: number } | null = null;
  @Input() zoom = 5;
  @Input() clustering = false;

  map!: google.maps.Map;
  marker!: google.maps.Marker;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['coords'] && changes['coords'].firstChange) {
      this.init();
    }

    if (changes['coords']) {
      this.updateMarker(changes['coords'].currentValue)
    }

    if (changes['zoom']) {
      this.map.setZoom(this.zoom)
    }

  }


  init() {
    this.map = new google.maps.Map(this.host.nativeElement, {
      center: { lat: -34.397, lng: 150.644 },
      zoom: this.zoom,
    });
  }

  updateMarker(coords: { lat: number; lng: number}) {
    this.map.setCenter(coords);

    if (this.marker) {
      this.marker.setMap(null);
    }
    this.marker = new google.maps.Marker({
      position: coords,
      map: this.map,
      title: "Hello World!",
    });
  }


}
