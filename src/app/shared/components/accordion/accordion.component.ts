import { Component, ContentChildren, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'app-accordion',
  template: `
    <div style="border: 3px solid red; padding: 10px">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements OnInit {
  @ContentChildren(AccordionGroupComponent) groups!: QueryList<AccordionGroupComponent>;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterContentInit() {
      this.groups.toArray().forEach(g => {
        g.opened = false;
        g.headerClick.subscribe(() => {
          this.closeAll();
          g.opened = true;
        })
      })
    this.groups.toArray()[0].opened = true;
  }

  closeAll() {
    this.groups.toArray().forEach(g => {
      g.opened = false;
    })
  }

}
