import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

/**
 * My Super awesome accordion group
 */
@Component({
  selector: 'app-accordion-group',
  template: `
    <div class="card mymodal">
      <div class="card-header bg-info text-white" (click)="headerClick.emit()">
        {{title}}
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div> 
  `,

})
export class AccordionGroupComponent {
  /**
   * Card Title
   */
  @Input() title: string = '';
  /**
   * if card is opened
   */
  @Input() opened = false;
  /**
   * user clicks header
   */
  @Output() headerClick = new EventEmitter();
}
