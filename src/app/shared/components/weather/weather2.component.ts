import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from './model/meteo';

@Component({
  selector: 'app-weather2',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>Meteo {{city}}</h1>
    <h2 *ngIf="meteo">{{meteo.main.temp}}°</h2>
  `,
})
export class Weather2Component  {
  _city: string | null = null;
  @Input() set city(val: string | null) {
    this._city = val;
    this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${val}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo = res
        this.cd.detectChanges()
      })
  }

  get city() {
    return this._city;
  }
  meteo: Meteo | null = null;

  constructor(
    private http: HttpClient,
    private cd: ChangeDetectorRef
  ) {
  }
/*
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['city']) {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => this.meteo = res)
    }
  }*/
}
