import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from './model/meteo';

@Component({
  selector: 'app-weather',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <!--  <input type="text" placeholder="search city">-->
    <h1 [style.color]="titleColor">
       {{city}} Meteo
    </h1>
    
    <h2 *ngIf="meteo">{{meteo.main.temp}}°</h2>
    
    <pre>{{config | json}}</pre>
  `,
})
export class WeatherComponent implements OnInit, OnChanges {
  @Input() city: string | null = null;
  @Input() titleColor: string = 'black';
  @Input() config: any
  meteo: Meteo | null = null;

  constructor(private http: HttpClient) {
    console.log('ctr', this.city)
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes', changes)
    if (changes['city']) {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => this.meteo = res)
    }

    if (changes['titleColor']) {
      console.log('color is changed')
    }
  }

  ngOnInit(): void {
    console.log('onInit', this.city)
  }

  ngAfterViewInit() {
    console.log('after view')
  }

  ngOnDestroy() {
    console.log('destroy')
  }


  render() {
    console.log('RENDER: WEATHER')
  }
}
