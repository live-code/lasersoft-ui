import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-row',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
})
export class RowComponent {
  @Input() mq: string = 'sm';


}
