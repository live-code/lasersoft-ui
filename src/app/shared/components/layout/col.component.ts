import { Component, HostBinding, Input, OnInit, Optional } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'app-col',
  template: `
    <ng-content></ng-content>
  `,
  styles: [
  ]
})
export class ColComponent {
  @Input() cols: number = 12;

  @HostBinding() get class() {
    if (this.row) {
      return `col-${this.row.mq}-${this.cols}`
    }
    return 'col';
  }
  constructor(@Optional() private row: RowComponent ) {

  }
}
