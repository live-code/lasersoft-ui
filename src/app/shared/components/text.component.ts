import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-text',
  template: `
      <ng-content></ng-content>
  `,
})
export class TextComponent {
  @Input() val: 1 | 2 | 3 | 4 | 5 = 3;
  @Input() color: 'primary' | 'success' | 'danger' = 'primary';

  @HostBinding() get class() {
    return `text-${this.color}`
  }
  @HostBinding('style.font-size') get fontSize() {
    return `${this.val * 10}px`
  }
}
