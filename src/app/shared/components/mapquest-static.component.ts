import { ChangeDetectionStrategy, Component, Input, SimpleChanges } from '@angular/core';

const API = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn'
@Component({
  selector: 'app-mapquest-static',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img
      *ngIf="city"
      width="100%"
      [src]="url" alt="">
  `,
  styles: [
  ]
})
export class MapquestStaticComponent {
  @Input() city: string | null = null;
  @Input() zoom: number = 5;
  url: string | null = null;
  rendered = false;

  ngOnInit() {
    if (!this.rendered) {
      console.log('init')
      this.drawMap();
    }

  }
  ngOnChanges(changes: SimpleChanges) {
    console.log('changes')
    this.drawMap();
    this.rendered = true;
  }

  drawMap() {
    this.url =  API + '&center=' + this.city + '&size=300,200&zoom=' + this.zoom
  }
}
