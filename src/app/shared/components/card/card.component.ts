import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card mymodal">
      <div class="card-header bg-dark text-white" (click)="opened = !opened">
        {{title}}
        <div class="pull-right">
          <i [class]="icon" (click)="iconClick.emit()"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content select="app-body"></ng-content>
      </div>
      
      <div class="card-footer">
        <ng-content select="app-footer"></ng-content>
      </div>
    </div>
  `,

})
export class CardComponent {
  @Input() icon: string | null = null;
  @Input() title: string = '';
  @Output() iconClick = new EventEmitter()
  opened = true;

}
