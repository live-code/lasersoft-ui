import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  template: `
    <ng-content></ng-content>
  `,
  styles: [
  ]
})
export class BodyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
