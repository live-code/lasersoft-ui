import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <ng-content></ng-content>
  `,
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
