import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BodyComponent } from './components/card/body.component';
import { FooterComponent } from './components/card/footer.component';
import { CardComponent } from './components/card/card.component';
import { WeatherComponent } from './components/weather/weather.component';
import { MapquestStaticComponent } from './components/mapquest-static.component';
import { GmapComponent } from './components/gmap.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { AccordionGroupComponent } from './components/accordion/accordion-group.component';
import { Weather2Component } from './components/weather/weather2.component';
import { PadDirectives } from './directives/pad.directives';
import { HelloDirective } from './directives/hello.directive';
import { MarginDirective } from './directives/margin.directive';
import { BorderDirective } from './directives/border.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { UrlDirective } from './directives/url.directive';
import { LinkDirective } from './directives/link.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { FocusDirective } from './directives/focus.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import { IfRoleIsDirective } from './directives/if-role-is.directive';
import { LoaderDirective } from './directives/loader.directive';
import { RowComponent } from './components/layout/row.component';
import { ColComponent } from './components/layout/col.component';
import { FlexDirective } from './directives/flex.directive';
import { FlexColDirective } from './directives/flex-col.directive';
import { TextComponent } from './components/text.component';
import { SeparatorComponent } from './components/separator.component';
import { FormatGbPipe } from './pipes/format-gb.pipe';
import { MapquestPipe } from './pipes/mapquest.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByTextPipe } from './pipes/filter-by-text.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { GetUserPipe } from './pipes/get-user.pipe';
import { GetUserHumanPipe } from './pipes/get-user-human.pipe';

@NgModule({
  declarations: [
    CardComponent,
    BodyComponent,
    FooterComponent,
    WeatherComponent,
    Weather2Component,
    MapquestStaticComponent,
    GmapComponent,
    AccordionComponent,
    AccordionGroupComponent,
    PadDirectives,
    HelloDirective,
    MarginDirective,
    BorderDirective,
    IfLoggedDirective,
    UrlDirective,
    LinkDirective,
    StopPropagationDirective,
    FocusDirective,
    IfSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    RowComponent,
    ColComponent,
    FlexDirective,
    FlexColDirective,
    TextComponent,
    SeparatorComponent,
    FormatGbPipe,
    MapquestPipe,
    FilterByGenderPipe,
    FilterByTextPipe,
    SortPipe,
    TimeAgoPipe,
    GetUserPipe,
    GetUserHumanPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    BodyComponent,
    FooterComponent,
    WeatherComponent,
    Weather2Component,
    MapquestStaticComponent,
    GmapComponent,
    AccordionComponent,
    AccordionGroupComponent,
    PadDirectives,
    HelloDirective,
    MarginDirective,
    BorderDirective,
    IfLoggedDirective,
    UrlDirective,
    LinkDirective,
    StopPropagationDirective,
    FocusDirective,
    IfSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    RowComponent,
    ColComponent,
    FlexDirective,
    FlexColDirective,
    TextComponent,
    SeparatorComponent,
    FormatGbPipe,
    MapquestPipe,
    FilterByGenderPipe,
    FilterByTextPipe,
    SortPipe,
    TimeAgoPipe,
    GetUserPipe,
    GetUserHumanPipe
  ]
})
export class SharedModule { }
