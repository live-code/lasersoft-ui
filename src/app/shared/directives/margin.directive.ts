import { Directive, ElementRef, Input, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appMargin]'
})
export class MarginDirective {
  @Input() set appMargin(val: number) {
    this.renderer.setStyle(this.el.nativeElement, 'margin', `${val * 10}px`)
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {
    console.log(el.nativeElement)
  }

}

// document.getElementById('text').style.fontSize = '20px'
// document.getElementById('text').style.margin = '20px'
