import { Directive, ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfSignin]'
})
export class IfSigninDirective {

  constructor(
    private tpl: TemplateRef<any>,
    private el: ElementRef,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {
    this.authService.ifLogged$
      .subscribe(val => {
        if (val) {
          view.createEmbeddedView(tpl)
        } else {
          view.clear();
        }
      })
  }

}
