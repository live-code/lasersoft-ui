import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() appIfRoleIs: 'admin' | 'moderator' | null = null;

  constructor(
    private tpl: TemplateRef<any>,
    private el: ElementRef,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {

  }

  ngOnInit() {
    this.authService.data$
      .subscribe(data => {
        if (data?.role === this.appIfRoleIs) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })
  }
}
