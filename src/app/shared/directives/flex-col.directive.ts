import { Directive, HostBinding, Input, Optional } from '@angular/core';
import { RowComponent } from '../components/layout/row.component';
import { FlexDirective } from './flex.directive';

@Directive({
  selector: '[appFlexCol]'
})
export class FlexColDirective {
  @Input() appFlexCol: number | string = 12
  @HostBinding() get class() {
    if (this.row) {
      return `col-${this.row.appFlex}-${this.appFlexCol}`
    }
    return 'col';
  }
  constructor(@Optional() private row: FlexDirective) {
  }
}
