import { ComponentRef, Directive, Input, Type, ViewContainerRef } from '@angular/core';
import { MapquestStaticComponent } from '../components/mapquest-static.component';
import { CardComponent } from '../components/card/card.component';
import { Widget } from '../../features/directive3/directive3.component';
import { Weather2Component } from '../components/weather/weather2.component';

const COMPONENTS: { [key: string]: Type<any> } = {
  'card': CardComponent,
  'map': MapquestStaticComponent,
  'weather2': Weather2Component,
}

@Directive({
  selector: '[appLoader]'
})
export class LoaderDirective {
  @Input('appLoader') widget!: Widget;

  constructor(private view: ViewContainerRef,) {}

  ngOnInit() {
    const compo: ComponentRef<any> = this.view.createComponent(COMPONENTS[this.widget.type])

    for (const key in this.widget.data) {
      compo.instance[key] = this.widget.data[key]
    }

  }

}
