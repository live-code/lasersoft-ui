import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appFlex]'
})
export class FlexDirective {
  @HostBinding() class = 'row';
  @Input() appFlex: 'sm' | 'md' | 'lg' = 'sm';

}
