import { Directive, ElementRef, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input() appUrl: string | null = null;
  @HostBinding('style.text-decoration') deco = 'underline'
  @HostBinding('style.color') color = 'orange'
  @HostBinding('style.cursor') cursor = 'pointer'

  constructor(
    private el: ElementRef
  ) {
    this.el.nativeElement.addEventListener('click', () => {
      if (this.appUrl) {
        window.open(this.appUrl)
      }
    })
  }

}
