import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHello]',
  exportAs: 'appHello'
})
export class HelloDirective {
  @HostBinding() class = 'alert alert-success'
  // @HostBinding() innerHTML = 'hello!!'
  @HostBinding('style.fontSize') fontSize = '40px'

  status = 'invalid'
}
