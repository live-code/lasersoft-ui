import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirectives {
  @Input('appPad') value: number = 1;
  @Input() color = 'black'

  @HostBinding('style.text-decoration') deco = 'underline'
  @HostBinding('style.padding') get padding() {
    return `${this.value * 10}px`
  }

  @HostBinding('style.color') get styleColor() {
    console.log('padding color')
    return this.color;
  }

  ngOnInit() {
    // console.log('ngOnInit', this.value)
  }
}
