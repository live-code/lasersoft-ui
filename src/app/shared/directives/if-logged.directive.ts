import { Directive, ElementRef, HostBinding, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective {
 /* @HostBinding() get hidden() {
    return !this.authService.data$.getValue();
  }*/

  constructor(
    private authService: AuthService,
    private el: ElementRef,
    private renderer: Renderer2
  ) {

    this.authService.ifLogged$
      .subscribe(val => {
        this.renderer.setStyle(
          this.el.nativeElement,
          'display',
          val ? '' : 'none'
        )
      })
  }

}
