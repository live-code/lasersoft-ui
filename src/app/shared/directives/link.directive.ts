import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appLink]'
})
export class LinkDirective {
  @Input() appLink: string | null = null;
  @HostBinding('style.text-decoration') deco = 'underline'
  @HostBinding('style.color') color = 'orange'
  @HostBinding('style.cursor') cursor = 'pointer'

  @HostListener('click')
  open() {
    if(this.appLink) {
      window.open(this.appLink)
    }
  }

}
