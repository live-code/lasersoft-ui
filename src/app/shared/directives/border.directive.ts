import { Directive, ElementRef, HostBinding, Input, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appBorder]'
})
export class BorderDirective {
  @Input() appBorder: string = 'solid';

  @HostBinding() class = 'p-1';

  /*
  @HostBinding('style.border') get border() {
    return `1px ${this.appBorder} black`
  }
 */

  ngOnChanges(changes: SimpleChanges) {
    if (changes['appBorder']) {
      this.renderer2.setStyle(
        this.el.nativeElement,
        'border',
        `1px ${this.appBorder} black`
      )
    }

  }

  constructor(
    private el: ElementRef,
    private renderer2: Renderer2
  ) {
    console.log('border')
  }

}
