import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="uikit1">1</button>
    <button routerLink="uikit2">2</button>
    <button routerLink="uikit3">3</button>
    <button routerLink="directives">directives</button>
    <button routerLink="directive2">directives2</button>
    <button routerLink="directive3">directives3</button>
    <button routerLink="pipes">pipes</button>
    <button routerLink="pipes2">pipes2</button>
    <button routerLink="pipes3">pipes3</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
}
