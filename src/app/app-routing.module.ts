import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'uikit1', loadChildren: () => import('./features/uikit-demo1/uikit-demo1.module').then(m => m.UikitDemo1Module) },
  { path: 'uikit2', loadChildren: () => import('./features/uikit-demo2/uikit-demo2.module').then(m => m.UikitDemo2Module) },
  { path: 'directives', loadChildren: () => import('./features/directives/directives.module').then(m => m.DirectivesModule) },
  { path: 'pipes', loadChildren: () => import('./features/pipes/pipes.module').then(m => m.PipesModule) },
  { path: '', redirectTo: 'uikit1', pathMatch: 'full'},
  { path: 'uikit3', loadChildren: () => import('./features/uikit-demo3/uikit-demo3.module').then(m => m.UikitDemo3Module) },
  { path: 'directive2', loadChildren: () => import('./features/directive2/directive2.module').then(m => m.Directive2Module) },
  { path: 'directive3', loadChildren: () => import('./features/directive3/directive3.module').then(m => m.Directive3Module) },
  { path: 'pipes2', loadChildren: () => import('./features/pipes2/pipes2.module').then(m => m.Pipes2Module) },
  { path: 'pipes3', loadChildren: () => import('./features/pipes3/pipes3.module').then(m => m.Pipes3Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
